package org.pdc.apps.rva.generator.util;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.BeanConfig;

/**
 * Public class to bootstrap the Swagger configuration
 *
 */
public class SwaggerBootstrap extends HttpServlet
{
  private static final long serialVersionUID = 3839373748128999880L;

  @Override
  public void init( ServletConfig config ) throws ServletException
  {
    super.init( config );

    BeanConfig beanConfig = new BeanConfig();
    beanConfig.setTitle( "FHP RVA Generator Service" );
    beanConfig.setDescription( "Welcome to FHP RVA Generator Service." );
    beanConfig.setContact( "yannick.guenet@gmail.com" );
    beanConfig.setVersion( "1.0.0" );
    beanConfig.setSchemes( new String[] {"http"} );
    beanConfig.setBasePath( "/fhp-rva-generator/v1" );
    beanConfig.setResourcePackage( "org.pdc.apps.rva.generator" );
    beanConfig.setScan( true );

  }
}