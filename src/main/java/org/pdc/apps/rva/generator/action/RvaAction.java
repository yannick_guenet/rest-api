package org.pdc.apps.rva.generator.action;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.pdc.apps.rva.generator.pojo.Data;

/**
 * Action Class for RVA Services
 * 
 * @author yguenet
 *
 */
public class RvaAction {
	private static Logger mLogger = Logger.getLogger(RvaAction.class);

	public RvaAction() {

	}

	/**
	 * Generate RVA JSON and return the JSON String
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public Data generate() {
		Data data = new Data();
		data.setMyFirstField("HELLO WORLD!");		
		mLogger.info("I was in generate method");
		return data;
	}

}
