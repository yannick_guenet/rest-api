package org.pdc.apps.rva.generator.resource;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.pdc.apps.rva.generator.action.RvaAction;
import org.pdc.apps.rva.generator.pojo.Data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Path("/rest")
@Api(value = "rest")
@Produces(MediaType.APPLICATION_JSON)
public class RvaServices {

	/**
	 * Resource endpoint to Generate RVA JSON FILE
	 * 
	 * 
	 * @return The result of generation
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	@GET
	@Path("/generate")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Generate RVA FHP", notes = "Return the FHP List")
	public Data generate(@Context UriInfo uriInfo) throws IOException, URISyntaxException {
		RvaAction action = new RvaAction();
		return action.generate();
	}

}
